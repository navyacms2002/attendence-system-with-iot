**Why Gitlab?**
GitLab helps us collect performance metrics for both servers as well as our applications. It quickly lets us determine the impact of merging a particular branch and keep an eye on our production systems without ever leaving GitLab.

Gitlab is not very tough to use as it seems so.

**Project Overview**
This column tells us about the project and what it is about. 
*Details* column give the details. 
*Activity* gives the actions performed.
*Releases* Releases are based on Git tags and mark specific points in a project's development history. They can contain information about the type of changes and can also deliver binaries, like compiled versions of your software.

**Repository**
1) Files : This options shows the files added in the repo.
2) Commits: A Git commit is a snapshot of the hierarchy (Git tree) and the contents of the files (Git blob) in a Git repository.
3) Branch: Git is like a tree.Branches are  simply a lightweight movable pointers to the commits.
4) Tags: Tags are ref's that point to specific points in Git history. Tagging is generally used to capture a point in history that is used for a marked version release.
5) Contributors: Shows how many contributions has been made by the one who has access to file.
6) Graph : shows a better view of contributions.

**Issues**
Issue or problem ping. 

**Analytics**
They show views and the analyitcs related to file.

**Wiki**
Knowledge base of Git.

A text in (**) is **bold**.
A text in (*) is *Italics*
A text in (\`) is `code`.

