• The speaker was very busy due to work from home and a baby. He had a constant urge to do something, to learn something new so he researched about skill learning in time.

• After reading a few books, he got to know that it requires 10000 hours to start learning something from scratch and being at the top of it. 

• People manipulated it to the statement that it requires 10000 hours to learn something. 

• So the speaker tells us that it only requires 20 hours to learn something. Only 20 hours with utmost concentration and focus. 

• So he wanted to learn how to play ukulele. He played his favourite song on stage and that was his 20th hour. 

• So he showed to us that  if we put all our focus and attention at learning something new, we can do it by giving just 45 mins a day in a month or 20 hours. 

• A skill can be learned in a few hours if we are extremely focused and concentrated else it can also take  months 
• Before doing something, we must plan it out, and practice it a lot 
• The speakers also tells us about the  learning curve
• More we practice and become good at it, it will less time to solve a problem  




How to achieve it? 

1)**Deconstruct the skill**
It means break it into small parts,  chapters. Practice important things. Learn about it to the root

2)**Learn enough to self correct**
It means make mistakes, and correct them on your own. Practice is important. Just reading is not enough.

3)**Remove Practice barriers**
Remove the distractions, internet, tv all the stuff that distracts you from your goal and assists you in procrastination . 

4)**Practice at least  20 hours**
Don't cut it short. To become skilled, you need to put in those 30 hours with utmost consideration. 
 

 